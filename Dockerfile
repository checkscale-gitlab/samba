FROM alpine:latest
LABEL maintainer="Mausy5043"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            bash \
            tzdata \
            samba-common-tools \
            samba-client \
            samba-server \
 && rm -rf /var/cache/apk/*

ENV TZ=Europe/Amsterdam

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
 && echo $TZ > /etc/timezone

EXPOSE 137/udp
EXPOSE 138/udp
EXPOSE 139/tcp
EXPOSE 445/tcp

VOLUME /srv/data

RUN mkdir -p /var/lib/samba \
 && touch /var/lib/samba/registry.tdb; \
    touch /var/lib/samba/account_policy.tdb; \
    touch /var/lib/samba/winbindd_idmap.tdb

#ENTRYPOINT ["smbd", "--foreground", "--log-stdout", "--no-process-group"]
CMD nmbd --foreground --log-stdout --no-process-group & \
    smbd --foreground --log-stdout --no-process-group
