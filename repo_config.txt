store=/tmp/samba/

# Don't change the settings below unless you know what you are doing!
service=samba
host="$(hostname)"
container="${service}_on_${host}"
image="mausy5043/${container}:latest"

source "${HOME}/.config/docker/${container}-store.txt" 2>/dev/null || echo "Not found: ${HOME}/.config/docker/${container}-store.txt"; echo

options+=("--name ${container}")
options+=("--restart unless-stopped")
options+=("-d")
options+=("-h ${container}")
options+=("-p 445:445")
options+=("-v ${HOME}/.config/docker/smb.conf:/etc/samba/smb.conf")
options+=("-v ${store}:/srv/data/")
